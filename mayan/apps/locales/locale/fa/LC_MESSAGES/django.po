# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Mehdi Amani <MehdiAmani@toorintan.com>, 2022
# Roberto Rosario, 2022
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-21 08:49+0000\n"
"PO-Revision-Date: 2022-02-03 10:13+0000\n"
"Last-Translator: Roberto Rosario, 2022\n"
"Language-Team: Persian (https://www.transifex.com/rosarior/teams/13584/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:28 events.py:5
msgid "Locales"
msgstr ""

#: events.py:8
msgid "User locale profile edited"
msgstr ""

#: links.py:18
msgid "Locale profile"
msgstr "پروفایل محلی"

#: links.py:25
msgid "Edit locale profile"
msgstr "ویرایش پروفایل محلی"

#: models.py:21
msgid "User"
msgstr "کاربر"

#: models.py:25
msgid "Timezone"
msgstr "زمان محلی"

#: models.py:28
msgid "Language"
msgstr "زبان"

#: models.py:34
msgid "User locale profile"
msgstr "پروفایل محلی کاربر"

#: models.py:35
msgid "User locale profiles"
msgstr "پروفایل محلی کاربر"

#: models.py:39
msgid "None"
msgstr "هیچ یک"

#: views.py:36
#, python-format
msgid "Locale profile for user: %s"
msgstr ""

#: views.py:82
#, python-format
msgid "Edit locale profile for user: %s"
msgstr ""
